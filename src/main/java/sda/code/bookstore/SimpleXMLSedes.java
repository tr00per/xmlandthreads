package sda.code.bookstore;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import sda.code.bookstore.model.Bookstore;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class SimpleXMLSedes {
    public static void main(String[] args) throws IOException {
        try (InputStream inp = SimpleXMLSedes.class.getResourceAsStream("/example.xml")) {
            ObjectMapper xmlMapper = new XmlMapper();
            Bookstore bookstore = xmlMapper.readValue(inp, Bookstore.class);

            System.out.println(bookstore);

            bookstore.getBooks().get(0).setTitle("Chronicles of Amber");

            xmlMapper.writeValue(new File("/tmp/bookstore.xml"), bookstore);
        }
    }
}
