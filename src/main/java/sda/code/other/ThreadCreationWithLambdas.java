package sda.code.other;

import java.util.concurrent.*;

public class ThreadCreationWithLambdas {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new Thread(() -> System.out.println(Thread.currentThread().getName() + " Cześć")).start();

        ExecutorService service = Executors.newCachedThreadPool();
        Future<Long> kiedyśLiczba = service.submit(() -> {
            System.out.println(Thread.currentThread().getName() + " Cześć");
            return System.nanoTime();
        });

        new MyThread("Mój wątek").start();

        System.out.println(Thread.currentThread().getName() + " Cześć");
        System.out.println("Kiedyś liczba: " + kiedyśLiczba.get());

        Future<Integer> one = service.submit(() -> 1);
        Future<Integer> plusOne = service.submit(() -> one.get() + 1);
        System.out.println("Najwolniejsze dodawanie świata: " + plusOne.get());

        CompletableFuture<Integer> newPlusOne = CompletableFuture
                .supplyAsync(() -> 1)
                .thenApply(integer -> integer + 1);
        System.out.println("Najwolniejsze dodawanie świata, ale z kompozycją: " + newPlusOne.join());
    }

    private static class MyThread extends Thread {
        public MyThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " Cześć!");
        }
    }
}
