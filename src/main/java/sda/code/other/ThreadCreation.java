package sda.code.other;

import java.util.concurrent.*;
import java.util.function.Function;
import java.util.function.Supplier;

public class ThreadCreation {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " Cześć");
            }
        }).start();

        ExecutorService service = Executors.newCachedThreadPool();
        Future<Long> kiedyśLiczba = service.submit(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                System.out.println(Thread.currentThread().getName() + " Cześć");
                return System.nanoTime();
            }
        });

        new MyThread("Mój wątek").start();

        System.out.println(Thread.currentThread().getName() + " Cześć");
        System.out.println("Kiedyś liczba: " + kiedyśLiczba.get());

        Future<Integer> one = service.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return 1;
            }
        });
        Future<Integer> plusOne = service.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return one.get() + 1;
            }
        });
        System.out.println("Najwolniejsze dodawanie świata: " + plusOne.get());

        CompletableFuture<Integer> newPlusOne = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                return 1;
            }
        })
        .thenApplyAsync(new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return integer + 1;
            }
        });
        System.out.println("Najwolniejsze dodawanie świata, ale z kompozycją: " + newPlusOne.join());
    }

    private static class MyThread extends Thread {
        public MyThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " Cześć!");
        }
    }
}
